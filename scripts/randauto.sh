#!/bin/bash

internal="eDP-1"
external="HDMI-1-0"

case "$1" in
  "noext") xrandr --output "$external" --off --output "$internal" --mode 1600x900 && /home/druida/.config/polybar/launch.sh 
    ;;
  "ext") xrandr --output "$internal" --mode 1600x900 --output "$external" --auto --right-of "$internal" && /home/druida/.config/polybar/launch.sh
    ;;
  *) echo "Use: [noext] disable external monitor | [ext] enable external monitor"
esac
