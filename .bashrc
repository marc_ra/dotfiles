# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias sudo='sudo -Es'
alias nvidia-settings='nvidia-settings --config="$XDG_CONFIG_HOME"/nvidia/settings'
alias yarn='yarn --use-yarnrc "$XDG_CONFIG_HOME/yarn/config"'
alias gitdot='git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
alias x2goclient="x2goclient --home=$HOME/.config"
alias kill='kill -9'
alias pkill='pkill -f -SIGKILL'
alias nc='netcat'
alias rofi='rofi -show drun'
alias feh='feh --scale-down'
alias glances='glances --disable-check-update'
alias mpv='mpv --autofit=1280x720'
alias rorphan='yay -Qdtq | yay -Rns -'
alias cal='cal -m'
alias df='df -x tmpfs'
alias reboot='/usr/bin/pkill chrome; systemctl reboot'
#alias emacs="emacsclient -n -a 'emacs' -c"
alias emacs="emacs -bg black"
#alias emacs="emacs --with-profile=default -bg black"
export XDG_STATE_HOME=$HOME/.local/state
export MOZ_X11_EGL=1
export XDG_CONFIG_HOME="$HOME/.config"
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export TERM=xterm-256color
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
#export HISTFILE="${XDG_STATE_HOME}"/bash/history
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export PATH="$HOME/.local/bin/:$PATH"
export PATH="$HOME/Downloads/android-studio/bin/:$PATH"
export XDG_DATA_DIRS="/usr/local/share:/usr/share:/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share:/mnt/sd/flatpak/exports/share/applications"
#export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export npm_config_prefix="$HOME/.local"
export GTK_THEME=Adwaita:dark
export LESSHISTFILE=-
export TERMINAL="alacritty"
export EDITOR="nvim"
export VISUAL="nvim"
export _JAVA_AWT_WM_NONREPARENTING=1
export HISTCONTROL=ignoredups:erasedups
export DXVK_STATE_CACHE_PATH="$HOME/.cache/dxvk"
export STEAM_COMPAT_DATA_PATH=/mnt/sd/proton
export STEAM_COMPAT_CLIENT_INSTALL_PATH=/mnt/sd/proton/installed

export HISTSIZE=1000
export HISTFILESIZE=2000
export LIBVA_DRIVER_NAME=iHD
export VDPAU_DRIVER=va_gl
export NVD_BACKEND=direct
export GBM_BACKEND=nvidia-drm
export XDG_CURRENT_DESKTOP=gtk

bind "set completion-ignore-case on"
WINEARCH=win32
export WINEPREFIX=/mnt/sd/wine6

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# lf icons
[ -f ~/.config/lf/lf_icons ] && {
  LF_ICONS="$(tr '\n' ':' <~/.config/lf/lf_icons)" &&
    export LF_ICONS
}

utf8() {
  iconv -f ISO-8859-1 -t UTF-8 $1 >$1.tmp
  rm $1
  mv $1.tmp $1
}
#history between windows
# Write history after each command
_bash_history_append() {
  builtin history -a
}

PROMPT_COMMAND="_bash_history_append; $PROMPT_COMMAND"
alias lf="lfcd"
lfcd() {
  # `command` is needed in case `lfcd` is aliased to `lf`
  cd "$(command lfrun -print-last-dir "$@")"
}

#export VKD3D_DISABLE_EXTENSIONS=VK_KHR_present_id,VK_KHR_present_wait
export ANDROID_HOME=/home/druida/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/platform-tools
export PATH="$PATH:$HOME/sdk/flutter/bin"
export JAVA_HOME='/opt/android-studio/jbr'
export npm_config_prefix="$HOME/.local"
export PATH=$JAVA_HOME/bin:$PATH
CHROME_EXECUTABLE=/usr/bin/google-chrome-stable
export CHROME_EXECUTABLE
#Go path
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
export WLR_RENDERER=vulkan
export WLR_NO_HARDWARE_CURSORS=1
eval "$(starship init bash)"
export PATH=$PATH:$HOME/.cargo/bin

fastfetch
#pfetch

# Load Angular CLI autocompletion.
source <(ng completion script)
