-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widet and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
local volume_widget = require('widgets.volume-widget.volume')
local ram_widget = require('widgets.ram-widget.ram-widget')

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")


-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(gears.filesystem.get_configuration_dir() .. "theme.lua")

-- Use correct status icon size
awesome.set_preferred_icon_size(32)
-- Notification mas width
beautiful.notification_max_width = 600

local separator = wibox.widget {
   widget = wibox.widget.separator,
   orientation = "vertical",
   forced_width = 30,
   color = "#ffffff",
   visible = true
}



-- Fix window snapping
awful.mouse.snap.edge_enabled = false
-- This is used later as the default terminal and editor to run.
terminal = "kitty"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey ="Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.floating,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.spiral,
}
-- }}}
--mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
--                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              --awful.menu.client_list({ theme = { width = 250 } })
						  popup:move_next_to(mouse.current_widget_geometry)
						  popup.visible = true
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2","3" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
		separator,
	    volume_widget{
		widget_type = 'arc'
	    },
	    ram_widget({
		widget_height = 50,
		widget_width= 50,
		color_used = "#FF0000"
		}),
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.systray(),
            mytextclock,
            s.mylayoutbox,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    -- Show hep
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
-- Move between tags
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),


    awful.key({}, "XF86AudioRaiseVolume", function () awful.util.spawn("amixer -D pulse sset Master 2%+", false) end),
    awful.key({}, "XF86AudioLowerVolume", function () awful.util.spawn("amixer -D pulse sset Master 2%-", false) end),
    awful.key({ modkey,           }, "Tab", function ()   awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    -- Master and column manipulation
    awful.key({ modkey    }, "v", function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "v",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey,  }, "b",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Shift" }, "b",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),



    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(    "request::activate", "key.unminimize", {raise = true} )
                  end
              end,
              {description = "restore minimized", group = "client"}),
    -- Set floating
     awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
                {description = "toggle floating", group = "client"}),
    -- Prompt
--    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
  --            {description = "run prompt", group = "launcher"}),
    awful.key({ modkey  }, "d", function () awful.spawn("dmenu_run -h 25") end), 


    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"})
)

clientkeys = gears.table.join(
    -- Handling window states
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),


    awful.key({ modkey,   }, "w",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),

-- Resize windows
    awful.key({ modkey, "Control" }, "Up", function (c)
      if c.floating then
        c:relative_move( 0, 0, 0, -10)
      else
        awful.client.incwfact(0.025)
      end
    end,
    {description = "Floating Resize Vertical -", group = "client"}),
    awful.key({ modkey, "Control" }, "Down", function (c)
      if c.floating then
        c:relative_move( 0, 0, 0,  10)
      else
        awful.client.incwfact(-0.025)
      end
    end,
    {description = "Floating Resize Vertical +", group = "client"}),
    awful.key({ modkey, "Control" }, "Left", function (c)
      if c.floating then
        c:relative_move( 0, 0, -10, 0)
      else
        awful.tag.incmwfact(-0.025)
      end
    end,
    {description = "Floating Resize Horizontal -", group = "client"}),
    awful.key({ modkey, "Control" }, "Right", function (c)
      if c.floating then
        c:relative_move( 0, 0,  10, 0)
      else
        awful.tag.incmwfact(0.025)
      end
    end,
    {description = "Floating Resize Horizontal +", group = "client"}),

    -- Moving floating windows
    awful.key({ modkey, "Shift"   }, "Down", function (c)
      c:relative_move(  0,  10,   0,   0) end,
    {description = "Floating Move Down", group = "client"}),
    awful.key({ modkey, "Shift"   }, "Up", function (c)
      c:relative_move(  0, -10,   0,   0) end,
    {description = "Floating Move Up", group = "client"}),
    awful.key({ modkey, "Shift"   }, "Left", function (c)
      c:relative_move(-10,   0,   0,   0) end,
    {description = "Floating Move Left", group = "client"}),
    awful.key({ modkey, "Shift"   }, "Right", function (c)
      c:relative_move( 10,   0,   0,   0) end,
    {description = "Floating Move Right", group = "client"}),



-- Maximize unmaximize
        awful.key({ modkey, "Control" }, "k",
                function (c)
                    c.maximized_vertical = not c.maximized_vertical
                    c:raise()
                end ,
                {description = "(un)maximize vertically", group = "client"}),
        awful.key({ modkey, "Control" }, "j",
                function (c)
                    c.maximized_vertical = not c.maximized_vertical
                    c:raise()
                end ,
                {description = "(un)maximize vertically", group = "client"}),
        awful.key({ modkey, "Control"   }, "l",
                function (c)
                    c.maximized_horizontal = not c.maximized_horizontal
                    c:raise()
                end ,
                {description = "(un)maximize horizontally", group = "client"}),
        awful.key({ modkey, "Control"   }, "h",
                function (c)
                    c.maximized_horizontal = not c.maximized_horizontal
                    c:raise()
                end ,
                {description = "(un)maximize horizontally", group = "client"}),

-- Moving window focus works between desktops
        awful.key({ modkey,           }, "j", function (c)
            awful.client.focus.global_bydirection("down")
            c:lower()
        end,
                {description = "focus next window up", group = "client"}),
        awful.key({ modkey,           }, "k", function (c)
            awful.client.focus.global_bydirection("up")
            c:lower()
        end,
                {description = "focus next window down", group = "client"}),
        awful.key({ modkey,           }, "l", function (c)
            awful.client.focus.global_bydirection("right")
            c:lower()
        end,
                {description = "focus next window right", group = "client"}),
        awful.key({ modkey,           }, "h", function (c)
            awful.client.focus.global_bydirection("left")
            c:lower()
        end,
                {description = "focus next window left", group = "client"}),


    -- Moving windows between positions works between desktops
    awful.key({ modkey, "Shift"   }, "h", function (c)
      awful.client.swap.global_bydirection("left")
      c:raise()
    end,
    {description = "swap with left client", group = "client"}),
    awful.key({ modkey, "Shift"   }, "l", function (c)
      awful.client.swap.global_bydirection("right")
      c:raise()
    end,
    {description = "swap with right client", group = "client"}),
    awful.key({ modkey, "Shift"   }, "j", function (c)
      awful.client.swap.global_bydirection("down")
      c:raise()
    end,
    {description = "swap with down client", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function (c)
      awful.client.swap.global_bydirection("up")
      c:raise()
    end,
    {description = "swap with up client", group = "client"}),

-- Maximize unmaximize
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}) --,
    --awful.key({ modkey, "Control" }, "m",
    --    function (c)
    --        c.maximized_vertical = not c.maximized_vertical
    --        c:raise()
    --    end ,
    --    {description = "(un)maximize vertically", group = "client"}),
    --awful.key({ modkey, "Shift"   }, "m",
    --    function (c)
    --        c.maximized_horizontal = not c.maximized_horizontal
    --        c:raise()
    --    end ,
    --    {description = "(un)maximize horizontally", group = "client"})

)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 3 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },
     { rule_any = {
          class = { "Steam" },
	  class = { "steam*" },
	  class = { "steam" }
      },
      properties = {
        titlebars_enabled = false,
        floating = true,
        border_width = 0,
        border_color = 0,
	hide_titlebars =true,
        size_hints_honor = false,
	fullscreen=true,
        }
    },
    -- Titlebars
    { rule_any = { type = { "dialog", "normal" } },
      properties = { titlebars_enabled = false } },

    {
	rule =	{ class = "jetbrains-pycharm-ce"},
	properties ={ floating = true }
    },
    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = false }
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

client.connect_signal("property::fullscreen", function(c)
  if c.fullscreen then
    gears.timer.delayed_call(function()
      if c.valid then
        c:geometry(c.screen.geometry)
      end
    end)
  end
end)
client.connect_signal("request::manage", function(client, context)

    if client.floating and context == "new" then
        client.placement = awful.placement.centered + awful.placement.no_overlap
    end
end)
-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)



times in msec
 clock   self+sourced   self:  sourced script
 clock   elapsed:              other lines

000.015  000.015: --- NVIM STARTING ---
000.673  000.658: locale set
001.495  000.822: inits 1
001.521  000.027: window checked
001.530  000.009: parsing arguments
007.680  006.149: expanding arguments
007.730  000.050: inits 2
009.102  001.372: init highlight
009.109  000.007: waiting for UI
010.775  001.666: done waiting for UI
010.806  000.031: init screen for UI
010.868  000.062: init default mappings
010.956  000.088: init default autocommands
012.316  000.143  000.143: sourcing /usr/share/nvim/runtime/ftplugin.vim
012.484  000.087  000.087: sourcing /usr/share/nvim/runtime/indent.vim
012.613  000.033  000.033: sourcing /usr/share/nvim/archlinux.vim
012.625  000.088  000.055: sourcing /etc/xdg/nvim/sysinit.vim
014.139  000.250  000.250: sourcing /usr/share/nvim/runtime/syntax/synload.vim
014.414  000.161  000.161: sourcing /usr/share/nvim/runtime/filetype.lua
031.102  016.638  016.638: sourcing /usr/share/nvim/runtime/filetype.vim
032.721  000.989  000.989: sourcing /usr/share/nvim/runtime/scripts.vim
032.821  020.074  002.036: sourcing /usr/share/nvim/runtime/syntax/syntax.vim
034.847  001.961  001.961: sourcing /usr/share/nvim/runtime/colors/pablo.vim
035.487  000.536  000.536: sourcing /usr/share/nvim/runtime/syntax/nosyntax.vim
035.661  000.124  000.124: sourcing /usr/share/nvim/runtime/syntax/synload.vim
035.772  000.868  000.208: sourcing /usr/share/nvim/runtime/syntax/syntax.vim
035.935  000.085  000.085: sourcing /usr/share/nvim/runtime/filetype.lua
035.979  000.018  000.018: sourcing /usr/share/nvim/runtime/filetype.vim
036.036  000.013  000.013: sourcing /usr/share/nvim/runtime/ftplugin.vim
036.085  000.011  000.011: sourcing /usr/share/nvim/runtime/indent.vim
038.178  002.034  002.034: sourcing /home/druida/.local/share/nvim/site/autoload/plug.vim
039.630  000.563  000.563: sourcing /usr/share/nvim/runtime/ftoff.vim
041.193  000.077  000.077: sourcing /usr/share/nvim/runtime/filetype.lua
047.294  006.077  006.077: sourcing /usr/share/nvim/runtime/filetype.vim
047.356  000.012  000.012: sourcing /usr/share/nvim/runtime/ftplugin.vim
047.400  000.008  000.008: sourcing /usr/share/nvim/runtime/indent.vim
053.011  005.565  005.565: sourcing /home/druida/.vim/plugged/gruvbox/colors/gruvbox.vim
053.032  040.358  002.993: sourcing /home/druida/.config/nvim/init.vim
053.036  001.405: sourcing vimrc file(s)
053.570  000.311  000.311: sourcing /home/druida/.vim/plugged/auto-pairs/plugin/auto-pairs.vim
055.097  001.345  001.345: sourcing /home/druida/.vim/plugged/Colorizer/autoload/Colorizer.vim
055.219  001.594  000.248: sourcing /home/druida/.vim/plugged/Colorizer/plugin/ColorizerPlugin.vim
055.521  000.073  000.073: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/init.vim
056.139  000.372  000.372: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/parts.vim
056.285  000.012  000.012: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/formatter/short_path.vim
057.151  000.117  000.117: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/util.vim
057.218  001.931  001.358: sourcing /home/druida/.vim/plugged/vim-airline/plugin/airline.vim
057.607  000.166  000.166: sourcing /usr/share/nvim/runtime/plugin/gzip.vim
057.637  000.008  000.008: sourcing /usr/share/nvim/runtime/plugin/health.vim
057.723  000.074  000.074: sourcing /usr/share/nvim/runtime/plugin/man.vim
058.186  000.192  000.192: sourcing /usr/share/nvim/runtime/pack/dist/opt/matchit/plugin/matchit.vim
058.277  000.539  000.348: sourcing /usr/share/nvim/runtime/plugin/matchit.vim
058.422  000.126  000.126: sourcing /usr/share/nvim/runtime/plugin/matchparen.vim
058.787  000.329  000.329: sourcing /usr/share/nvim/runtime/plugin/netrwPlugin.vim
058.917  000.007  000.007: sourcing /home/druida/.local/share/nvim/rplugin.vim
058.923  000.111  000.104: sourcing /usr/share/nvim/runtime/plugin/rplugin.vim
059.020  000.080  000.080: sourcing /usr/share/nvim/runtime/plugin/shada.vim
059.055  000.018  000.018: sourcing /usr/share/nvim/runtime/plugin/spellfile.vim
059.205  000.114  000.114: sourcing /usr/share/nvim/runtime/plugin/tarPlugin.vim
059.282  000.057  000.057: sourcing /usr/share/nvim/runtime/plugin/tohtml.vim
059.312  000.013  000.013: sourcing /usr/share/nvim/runtime/plugin/tutor.vim
059.465  000.138  000.138: sourcing /usr/share/nvim/runtime/plugin/zipPlugin.vim
059.679  001.034: loading rtp plugins
059.780  000.100: loading packages
060.037  000.257: loading after plugins
060.047  000.010: inits 3
060.836  000.789: reading ShaDa
061.183  000.189  000.189: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions.vim
061.283  000.034  000.034: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/quickfix.vim
061.427  000.105  000.105: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline.vim
061.546  000.021  000.021: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/netrw.vim
061.722  000.037  000.037: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/section.vim
061.945  000.126  000.126: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/highlighter.vim
062.162  000.546  000.383: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/term.vim
067.014  000.039  000.039: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/nvimlsp.vim
067.165  000.070  000.070: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/whitespace.vim
067.363  000.036  000.036: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/po.vim
067.474  000.054  000.054: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/wordcount.vim
067.615  000.020  000.020: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/keymap.vim
067.730  000.025  000.025: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/searchcount.vim
071.600  000.034  000.034: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/themes.vim
072.558  001.038  001.005: sourcing /home/druida/.vim/plugged/gruvbox/autoload/airline/themes/gruvbox.vim
073.679  002.200  001.161: sourcing /home/druida/.vim/plugged/gruvbox/autoload/airline/themes/gruvbox.vim
088.685  000.083  000.083: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/builder.vim
088.947  000.050  000.050: sourcing /home/druida/.vim/plugged/vim-airline/autoload/airline/extensions/default.vim
122.903  058.597: opening buffers
123.543  000.640: BufEnter autocommands
123.546  000.003: editing files in windows
123.628  000.082: VimEnter autocommands
123.630  000.002: UIEnter autocommands
123.637  000.006: before starting main loop
124.788  001.151: first screen update
124.790  000.002: --- NVIM STARTED ---
