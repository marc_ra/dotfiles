local cmp = require("cmp")

require("luasnip.loaders.from_vscode").lazy_load()

require("luasnip.loaders.from_vscode").load({ include = { "html" } })
require("luasnip").filetype_extend("typescript", { "javascript" }, "html")
require("luasnip.loaders.from_snipmate").lazy_load()

local list_snips = function()
	local ft_list = require("luasnip").available()[vim.o.filetype]
	local ft_snips = {}
	for _, item in pairs(ft_list) do
		ft_snips[item.trigger] = item.name
	end
	print(vim.inspect(ft_snips))
end

vim.api.nvim_create_user_command("SnipList", list_snips, {})
--Check if snippet folder exists
if vim.fn.isdirectory("~/vscode-angular-snippets/snippets/") then
	require("luasnip.loaders.from_vscode").lazy_load({ paths = { "~/Templates/vscode-angular-snippets/snippets/" } })
end

local has_words_before = function()
	local line, col = unpack(vim.api.nvim_win_get_cursor(0))
	return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end
--local kind_icons = require('plugins.utils').kind_icons
local luasnip = require("luasnip")

vim.opt.completeopt = "menu,menuone,noselect"
cmp.setup({
	snippet = {
		expand = function(args)
			-- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
			luasnip.lsp_expand(args.body) -- For `luasnip` users.
			-- require('snippy').expand_snippet(args.body) -- For `snippy` users.
			-- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
		end,
	},
	completion = {
		completeopt = "menu,menuone,noinsert",
	},
	formatting = {
		fields = { "abbr", "kind", "menu" },
		format = require("lspkind").cmp_format({
			mode = "symbol", -- show only symbol annotations
			maxwidth = 50,
			ellipsis_char = "...",
		}),
	},
	mapping = {
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			elseif has_words_before() then
				cmp.complete()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
		["<C-e>"] = cmp.mapping({
			i = cmp.mapping.abort(),
			c = cmp.mapping.close(),
		}),
		["<CR>"] = cmp.mapping.confirm({ select = true }),
	},
	sources = cmp.config.sources({
		{ name = "luasnip" },
		{ name = "nvim_lsp" },
		{ name = "buffer" },
		{ name = "css_vars" },
		--{ name = "vsnip" ,priority = 300},
		--{ name = "nvim_lua" },
		{ name = "path" },
		-- { name = 'nvim_lsp_signature_help' },
	}, {}),
})
