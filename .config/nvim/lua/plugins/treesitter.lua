require 'nvim-treesitter.configs'.setup {
  autotag = {
    enable = false,
  },
  -- A list of parser names, or "all" (the five listed parsers should always be installed)
  ensure_installed = { "lua", "go", "javascript", "typescript", "vim", "vimdoc", "query", "html" },

  indent = {
    enable = true
  },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  highlight = {
    enable = true,
    use_languagetree = true,
    --additional_vim_regex_highlighting = true,
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    --additional_vim_regex_highlighting = { 'htmlangular' }/,
  },
  rainbow = {
    enable = true,
    extended_mode = true,
    -- list of languages you want to disable the plugin for
    --disable = { 'jsx', 'cpp' },
    -- Which query to use for finding delimiters
    query = 'rainbow-parens',
    -- Highlight the entire buffer all at once
    -- strategy = require('ts-rainbow').strategy.global,
  },
  refactor = {
    highlight_definitions = {
      enable = true,
      -- Set to false if you have an `updatetime` of ~100.
      clear_on_cursor_move = true,
    },
    smart_rename = {
      enable = true,
      -- Assign keymaps to false to disable them, e.g. `smart_rename = false`.
      keymaps = {
        smart_rename = "grr",
      },
    },
  },
}
