require("mason").setup()

require("mason-lspconfig").setup({
	ensure_installed = {
		-- Replace these with whatever servers you want to install
		"vtsls",
		"emmet_language_server",
		"angularls",
		"lua_ls",
	},
})

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())

local lsp_attach = function(client, bufnr)
	if client.server_capabilities.documentHighlightProvider then
		vim.api.nvim_exec2(
			[[
	    augroup lsp_document_highlight
	    autocmd! * <buffer>
	    autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
	    autocmd CursorHoldI <buffer> lua vim.lsp.buf.document_highlight()
	    autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
	    autocmd CursorHold * lua vim.diagnostic.open_float(nil, {focus=false})
	    augroup END
	]],
			{}
		)
	end
	local opts = { buffer = bufnr, remap = false }

	vim.keymap.set("n", "gr", function()
		vim.lsp.buf.rename()
	end, opts)
	--vim.keymap.set('v', 'gp', vim.lsp.buf.code_action, opts)
	vim.keymap.set("n", "ga", function()
		require("fastaction").code_action()
	end, opts)
	vim.keymap.set("v", "ga", function()
		require("fastaction").range_code_action()
	end, opts)
	vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
	vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
	vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
	vim.keymap.set("n", "gI", vim.lsp.buf.implementation, opts)
	--  vim.keymap.set('n', 'go', vim.lsp.buf.type_definition, opts)
	vim.keymap.set("n", "go", "<cmd>OrganizeImports<cr>", opts)
	vim.keymap.set("n", "gi", function()
		vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
	end, opts)
	vim.keymap.set("n", "gl", require("telescope.builtin").lsp_references, opts)
	vim.keymap.set("n", "gp", require("telescope.builtin").live_grep, opts)
	vim.keymap.set("n", "gs", vim.lsp.buf.signature_help, opts)
	--vim.keymap.set({ 'n', 'x' }, '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
	vim.keymap.set({ "n" }, "<F3>", function()
		require("conform").format()
	end, opts)

	vim.keymap.set({ "v", "x" }, "<F3>", function()
		require("conform").format({ lsp_fallback = true })
	end, opts)
	vim.keymap.set("n", "gf", vim.diagnostic.open_float, opts)
	vim.keymap.set("n", "[", vim.diagnostic.goto_next, opts)
	vim.keymap.set("n", "]", vim.diagnostic.goto_prev, opts)
	-- Create your keybindings here...
end

local lspconfig = require("lspconfig")
require("mason-lspconfig").setup_handlers({
	function(server_name)
		lspconfig[server_name].setup({
			on_attach = lsp_attach,
			capabilities = capabilities,
		})
	end,
})

local util = require("lspconfig/util")
lspconfig.rust_analyzer.setup({
	on_attach = lsp_attach,
	capabilities = capabilities,
	filetypes = { "rust" },
	root_dir = util.root_pattern("Cargo.toml"),

	cmd = { "rustup", "run", "stable", "rust-analyzer" },
	settings = {
		["rust_analyzer"] = {
			allFeatures = true,
			diagnostics = {
				enable = true,
				disabled = { "unresolved-procmacro" },
				enableExperimental = true,
			},
		},
	},
})

lspconfig.pyright.setup({
	capabilities = capabilities,
	on_attach = lsp_attach,
	filetypes = { "python" },
})

lspconfig.emmet_language_server.setup({
	capabilities = capabilities,
	on_attach = lsp_attach,
	filetypes = { "javascriptreact", "typescriptreact","html","css" },
})

lspconfig.lua_ls.setup({
	capabilities = capabilities,
	on_attach = lsp_attach,
	settings = {
		Lua = {
			runtime = {
				-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
				version = "LuaJIT",
			},
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				globals = { "vim" },
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file("", true),
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = {
				enable = false,
			},
			hint = {
				enable = true,
			},
		},
	},
})
capabilities.textDocument.completion.completionItem.snippetSupport = true

lspconfig.gopls.setup({
	cmd = { "gopls" },
	filetypes = { "go", "gomod", "gowork", "gotmpl" },
	root_dir = util.root_pattern("go.work", "go.mod", ".git"),
	-- for postfix snippets and analyzers
	capabilities = capabilities,
	on_attach = lsp_attach,
	settings = {
		gopls = {
			completeUnimported = true,
			usePlaceholders = true,
			staticcheck = true,
			analyses = {
				unusedparams = true,
				shadow = true,
			},
			hints = {
				ssignVariableTypes = true,
				compositeLiteralFields = true,
				compositeLiteralTypes = true,
				constantValues = true,
				functionTypeParameters = true,
				parameterNames = true,
				rangeVariableTypes = true,
			},
		},
	},
})

lspconfig.angularls.setup({
	capabilities = capabilities,
	on_attach = lsp_attach,
	filetypes = { "html" },
  single_file_support = true
})

lspconfig.typos_lsp.setup({})

lspconfig.html.setup({
	capabilities = capabilities,
	on_attach = lsp_attach,
	filetypes = { "html" },
	init_options = {
		configurationSection = { "html" },
		embeddedLanguages = {
			css = true,
			javascript = true,
		},
		provideFormatter = true,
	},
	settings = {},
	single_file_support = true,
})

local function organize_imports()
	local params = {
		command = "_typescript.organizeImports",
		arguments = { vim.api.nvim_buf_get_name(0) },
		title = "",
	}
	vim.lsp.buf.execute_command(params)
end

lspconfig.vtsls.setup({
	on_attach = lsp_attach,
	capabilities = capabilities,
	commands = {
		OrganizeImports = {
			organize_imports,
			description = "Organize Imports",
		},
	},
	settings = {
		javascript = {
			inlayHints = {
				includeInlayParameterNameHints = "all",
				includeInlayParameterNameHintsWhenArgumentMatchesName = true,
				includeInlayFunctionParameterTypeHints = true,
				includeInlayVariableTypeHints = true,
				includeInlayVariableTypeHintsWhenTypeMatchesName = true,
				includeInlayPropertyDeclarationTypeHints = true,
				includeInlayFunctionLikeReturnTypeHints = true,
				includeInlayEnumMemberValueHints = true,
			},
		},
		typescript = {
			inlayHints = {
				includeInlayParameterNameHints = "all",
				includeInlayParameterNameHintsWhenArgumentMatchesName = true,
				includeInlayFunctionParameterTypeHints = true,
				includeInlayVariableTypeHints = true,
				includeInlayVariableTypeHintsWhenTypeMatchesName = true,
				includeInlayPropertyDeclarationTypeHints = true,
				includeInlayFunctionLikeReturnTypeHints = true,
				includeInlayEnumMemberValueHints = true,
			},
		},
	},
})
require("lsp_signature").setup()
