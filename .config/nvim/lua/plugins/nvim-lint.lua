local status_ok, lint = pcall(require, "lint")
if not status_ok then
  return
end

lint.linters_by_ft = {
  javascript = { "eslint_d" },
  typescript = { "eslint_d" },
  typescriptreact = { "eslint_d" },
  javascriptreact = { "eslint_d" },
}
local lint_autogroup = vim.api.nvim_create_augroup("lint", { clear = true })
vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost", "InsertLeave" }, {
  pattern= {"*.js","*.ts",".tsx"},
  group = lint_autogroup,
  callback = function()
    lint.try_lint()
  end
})
