local status_ok, conform = pcall(require, "conform")
if not status_ok then
	return
end

conform.setup({
	formatters_by_ft = {
		javascript = { "prettier", stop_after_first = true },
		typescript = { "prettier", stop_after_first = true },
		typescriptreact = { "prettier", stop_after_first = true },
		javascriptreact = { "prettier", stop_after_first = true },
		htmlangular = { "prettier" },
		html = { "prettier" },
		css = { "prettier", stop_after_first = true },
		lua = { "stylua" },
		go = { "goimports", "gofmt" },
		bash = { "shfmt", stop_after_first = true },
		sh = { "shfmt", stop_after_first = true },
	},
})
