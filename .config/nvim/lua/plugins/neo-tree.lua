require("neo-tree").setup({

	renderers = {
		directory = {
			{ "indent" },
			{ "icon" },
			{ "current_filter" },
			{ "name" },
			{ "clipboard" },
			{ "diagnostics", errors_only = true },
		},
		file = {
			{ "indent" },
			{ "icon" },
			{
				"name",
				use_git_status_colors = true,
				zindex = 10,
			},
			{ "clipboard" },
			{ "bufnr" },
			{ "modified" },
			{ "diagnostics" },
			{ "git_status" },
		},
	},
	close_if_last_window = true,
	filesystem = {
		follow_current_file = {
			enabled = true, -- This will find and focus the file in the active buffer every time
			--               -- the current file is changed while the tree is open.
			leave_dirs_open = false, -- `false` closes auto expanded dirs, such as with `:Neotree reveal`
		},
	},
	buffers = {
		follow_current_file = {
			enabled = true,
		},
	},
	default_component_configs = {
		container = {
			enable_character_fade = true,
		},
	},
	event_handlers = {
		{
			event = "file_opened",

			handler = function(_)
				require("neo-tree").close_all()
			end,
		},
	},
	window = {
		width = 30,
	},
})
