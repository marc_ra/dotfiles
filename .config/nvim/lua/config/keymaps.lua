vim.g.mapleader = " "

local opts = { noremap = true, silent = true }
local keymap = vim.api.nvim_set_keymap
vim.keymap.set("n", "<leader>.", "<cmd>NvimTreeToggle<cr>")
vim.keymap.set("n", "?",  require "nvim-tree.api".tree.toggle_help, opts)
local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>b", builtin.buffers)
vim.keymap.set("n", "<leader>f", builtin.find_files)
local ng = require("ng");
vim.keymap.set("n", "<leader>at", function() ng.goto_template_for_component({ reuse_window = true }) end, opts)
vim.keymap.set("n", "<leader>ac", function() ng.goto_component_with_template_file({ reuse_window = true }) end, opts)

vim.keymap.set('n', '<A-Tab>', '<Cmd>BufferLineCycleNext<CR>', opts)
vim.keymap.set('n', '<A-Tab>', '<Cmd>BufferLineCyclePrev<CR>', opts)



-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

--remapek arrow keyse
keymap("i", "<C-h>", "<Left>", opts)
keymap("i", "<C-l>", "<Right>", opts)
keymap("i", "<C-j>", "<Down>", opts)
keymap("i", "<C-k>", "<Up>", opts)

-- Visual Block --
-- Move text up and down
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)

--local keyset = vim.keymap.set
-- Autocomplete
function _G.check_back_space()
  local col = vim.fn.col('.') - 1
  return col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') ~= nil
end

--
vim.g.go_doc_keywordprg_enabled = 0
vim.g.go_def_mapping_enabled = 0
