vim.cmd([[autocmd CursorHold <buffer> lua vim.diagnostic.open_float({focusable = false})]])
local api = vim.api

api.nvim_create_autocmd("TextYankPost", {
  callback = function()
    vim.highlight.on_yank()
  end
})
vim.filetype.add({
  pattern = { [".*/hyprland%.conf"] = "hyprlang" },
})

vim.filetype.add({
  pattern = {
    [".*%.component%.html"] = "htmlangular", -- Sets the filetype to `htmlangular` if it matches the pattern
  },
})
vim.cmd('runtime! ftplugin/html.vim!')

