local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end

vim.opt.rtp:prepend(vim.env.LAZY or lazypath)

require("lazy").setup({
	defaults = {
		-- By default, only LazyVim plugins will be lazy-loaded. Your custom plugins will load during startup.
		-- If you know what you're doing, you can set this to `true` to have all your custom plugins lazy-loaded by default.
		lazy = true,
		-- It's recommended to leave version=false for now, since a lot the plugin that support versioning,
		-- have outdated releases, which may break your Neovim install.
		version = false, -- always  the latest git commit
		-- version = "*", -- try installing the latest stable version for plugins that support semver
	},
	change_detection = {
		-- automatically check for config file changes and reload the ui
		enabled = true,
		notify = true, -- get a notification when changes are found
	},
	spec = {
		-- add LazyVim and import its plugins
		-- { "LazyVim/LazyVim", import = "lazyvim.plugins" },
		-- import any extras modules here
		-- { import = "lazyvim.plugins.extras.lang.typescript" },
		-- { import = "lazyvim.plugins.extras.lang.json" },
		-- { import = "lazyvim.plugins.extras.ui.mini-animate" },
		-- import/override with your plugins
		--{ import = "plugins" },
		-- {
		-- 	"nvim-neo-tree/neo-tree.nvim",
		-- 	branch = "v3.x",
		-- 	dependencies = {
		-- 		"nvim-lua/plenary.nvim",
		-- 		"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
		-- 		"MunifTanjim/nui.nvim",
    -- 	},
    -- },
    -- 
    {
      "nvim-tree/nvim-tree.lua",
      dependencies = {
        "nvim-tree/nvim-web-devicons",
      },
      config= function ()
      end,
      lazy=false
    },
    {
      "lukas-reineke/indent-blankline.nvim",
      main = "ibl",
      enabled = true,
    },
    {
      "folke/tokyonight.nvim",
      lazy = false,
      priority = 1000,
      opts = {},
      config = function()
        vim.cmd([[colorscheme tokyonight]])
      end,
    },
    --Telescope
    {
      "nvim-telescope/telescope.nvim",
      tag = "0.1.8",
      dependencies = { "nvim-lua/plenary.nvim" },
      lazy = true,
    },
    {
      "nvim-treesitter/nvim-treesitter",
      build = ":TSUpdate",
      config = function()
        for _, group in ipairs(vim.fn.getcompletion("@lsp", "highlight")) do
          vim.api.nvim_set_hl(0, group, {})
        end
      end,
    },
    {
      "neovim/nvim-lspconfig",
      dependencies = {
        { -- Optional
          "williamboman/mason.nvim",
          build = function()
            pcall(vim.cmd, "MasonUpdate")
          end,
          lazy = true,
          opts = {
            inlay_hint = { enabled = true },
          },
        },
        { "hrsh7th/cmp-nvim-lsp" },
        { "williamboman/mason-lspconfig.nvim", lazy = true }, -- Optional
      },
    }, -- Required
    -- Autocompletion
    {
      "hrsh7th/nvim-cmp",
      event = "InsertEnter",
      dependencies = {
        { "hrsh7th/cmp-nvim-lsp" },
        { "hrsh7th/cmp-buffer" },
        { "hrsh7th/cmp-path" },
        { "hrsh7th/cmp-cmdline" },
        {"jdrupal-dev/css-vars.nvim"},
        {
          "L3MON4D3/LuaSnip",
          -- follow latest release.
          version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
          -- install jsregexp (optional!).
          build = "make install_jsregexp",
          dependencies = {
            "rafamadriz/friendly-snippets",
            lazy = true,
          },
        },
        "saadparwaiz1/cmp_luasnip",
        "onsails/lspkind.nvim",
        "rafamadriz/friendly-snippets",
      },
    }, -- Required
    { "hrsh7th/cmp-nvim-lsp" }, -- Required

    {
      "fatih/vim-go",
      event = "VeryLazy",
      ft = "go",
      enabled = false,
    },
    {
      "ray-x/lsp_signature.nvim",
      event = "VeryLazy",
    },
    {
      "stevearc/conform.nvim",
    },
    {
      "windwp/nvim-autopairs",
      event = "InsertEnter",
      config = true,
    },
    {
      "max397574/better-escape.nvim",
      config = function()
        require("better_escape").setup()
      end,
      lazy = false,
    },
    {
      "windwp/nvim-ts-autotag",
      event = "InsertEnter",
      ft = { "go", "js", "tsx" },
    },

    -- Snippets
    -- {'mhartington/vim-angular2-snippets'}
    --
    {
      "HiPhish/rainbow-delimiters.nvim",
      --dir = "~/.local/share/nvim/lazy/rainbow-delimiters.nvim",
      event = { "InsertEnter", "VeryLazy" },
      ft = { "js", "go", "ts", "tsx", "html" },
      lazy = false,
      enabled = true,
    },
    { "rcarriga/nvim-notify", event = "InsertEnter" },
    { "lewis6991/gitsigns.nvim", ft = { "html", "go", "js" } },
    {
      "kosayoda/nvim-lightbulb",
      ft = { "html", "go", "jsx", "js" },
      dependencies = {
        "antoinemadec/FixCursorHold.nvim",
      },
    },
    {
      "nvim-lualine/lualine.nvim",
      event = "VeryLazy",
      dependencies = {
        { "nvim-tree/nvim-web-devicons", lazy = true },
      },
      config = function()
        require("lualine").setup({})
      end,
    },
    -- {
    --   "akinsho/bufferline.nvim",
    --   version = "*",
    --   dependencies = { "nvim-tree/nvim-web-devicons", lazy = true },
    -- },
      {'romgrk/barbar.nvim',
    dependencies = {
      'lewis6991/gitsigns.nvim', -- OPTIONAL: for git status
      'nvim-tree/nvim-web-devicons', -- OPTIONAL: for file icons
    },
    init = function() vim.g.barbar_auto_setup = false end,
    opts = {
      -- lazy.nvim will automatically call setup for you. put your options here, anything missing will use the default:
      -- animation = true,
      -- insert_at_start = true,
      -- …etc.
    },
    version = '^1.0.0', -- optional: only update when a new 1.x version is released
  },
    {
      "numToStr/Comment.nvim",
      config = function()
        require("Comment").setup({})
      end,
    },
    {
      "Chaitanyabsprip/fastaction.nvim",
      ---@type FastjctionConfig
      opts = {},
    },
    {
      "joeveiga/ng.nvim",
      event = "VeryLazy",
    },
    {
      "dlvandenberg/tree-sitter-angular",
      lazy = false,
      config = function() end,
    },
    {
      "brenoprata10/nvim-highlight-colors",
      event = "VeryLazy",
    },
    -- {
    --   dir = "~/Projects/scratch/",
    --   name = "scratch-buffer",
    --   config = function()
    --     require("scratch-buffer")
    --   end,
    --   lazy = false,
    -- },
    {
      "max397574/colortils.nvim",
      cmd = "Colortils",
      config = function()
        require("colortils").setup({
          -- Some mappings which are used inside the tools
          mappings = {
            -- replace the color under the cursor with the current color in the format specified above
            replace_default_format = "<leader>p",
            -- replace the color under the cursor with the current color in a format you can choose
            replace_choose_format = "<leader>c",
          },
        })
      end,
    },
    {
      "kylechui/nvim-surround",
      version = "*", -- Use for stability; omit to use `main` branch for the latest features
      event="VeryLazy",
      config = function()
        require("nvim-surround").setup({})
      end,
      lazy = false,
    },
    {
      'famiu/bufdelete.nvim'
    }
  },
  checker = { enabled = true }, -- automatically check for plugin updates
  performance = {
    rtp = {
      -- disable some rtp plugins
      disabled_plugins = {
        "2html_plugin",
        "tohtml",
        "getscript",
        "getscriptPlugin",
        "gzip",
        "logipat",
        "netrw",
        "netrwPlugin",
        "netrwSettings",
        "netrwFileHandlers",
        "matchit",
        "tar",
        "tarPlugin",
        "rrhelper",
        "spellfile_plugin",
        "vimball",
        "vimballPlugin",
        "zip",
        "zipPlugin",
        "tutor",
        "rplugin",
        "syntax",
        "synmenu",
        "optwin",
        "compiler",
        "bugreport",
        "ftplugin",
      },
    },
  },
})
