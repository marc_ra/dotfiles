require("config.options")
require("config.lazy")
require("config.autocmd")
require("config.keymaps")
require("plugins.notify")
require("plugins.telescope")
require("plugins.lsp.lsp")
require("plugins.cmp")
--require("plugins.neo-tree")
require("plugins.treesitter")
require("plugins.autopairs")
require("plugins.autotag")
require("plugins.gitsigns")
require("plugins.light-bulb")
require("plugins.bufferline")
require("plugins.nvim-colorizer")
require("plugins.hyprls")
require("plugins.indent-blankline")
require("plugins.conform")
require("plugins.nvim-tree")

