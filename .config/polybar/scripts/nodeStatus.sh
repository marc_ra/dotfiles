#!/bin/bash
#
# prints tiled or monocle status for polybar
state=$(bspc query -T -d focused| jq -r '.layout')

case "$state" in
    tiled)
        echo "||" # means tiled
    ;;
    monocle)
        echo "[  ]" # means monocle
    ;;
esac
