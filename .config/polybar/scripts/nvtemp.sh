#!/bin/bash
#prints nvdia gpu temperature for polybar

RESULT=$(nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader)

exit=$?

if [ $exit -ne 0 ]; then

  echo "E?"
  exit
fi

echo $RESULT
